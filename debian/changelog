rust-async-channel (2.3.1-9) unstable; urgency=medium

  * drop patch 2001, obsoleted by Debian package changes
  * stop mention dh-cargo in long description
  * declare rust-related build-dependencies unconditionally,
    i.e. drop broken nocheck annotations
  * add metadata about upstream project
  * update git-buildpackage config: simplify usage comments
  * update watch file:
    + improve filename mangling
    + use Github API
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 06 Feb 2025 09:45:26 +0100

rust-async-channel (2.3.1-8) unstable; urgency=medium

  * reduce autopkgtest to check single-feature tests only on amd64
  * tighten build- and autopkgtest-dependencies for crate futures-lite

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 17 Aug 2024 08:06:27 +0200

rust-async-channel (2.3.1-7) unstable; urgency=medium

  * skip feature tests on s380x;
    closes: bug#1077474, thanks to Matthias Geiger

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 29 Jul 2024 19:03:30 +0200

rust-async-channel (2.3.1-6) unstable; urgency=medium

  * autopkgtest-depend on dh-rust (not dh-cargo)
  * skip autopkgtest without features on armel;
    closes: bug#1077327, thanks to Matthias Geiger

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 28 Jul 2024 16:27:31 +0200

rust-async-channel (2.3.1-5) unstable; urgency=medium

  * fix autopkgtest-dependencies;
    closes: bug#1076477, thanks to Peter Michael Green

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 17 Jul 2024 01:19:03 +0200

rust-async-channel (2.3.1-4) unstable; urgency=medium

  * no-changes source-only upload to enable testing migration

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 14 Jul 2024 16:12:26 +0200

rust-async-channel (2.3.1-3) experimental; urgency=medium

  * simplify packaging;
    build-depend on dh-sequence-rust
    (not dh-cargo libstring-shellquote-perl)

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 13 Jul 2024 09:06:20 +0200

rust-async-channel (2.3.1-2) experimental; urgency=medium

  * drop patch 2001_concurrent-queue;
    tighten (build-)dependencies for crate concurrent-queue

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 12 Jun 2024 11:30:12 +0200

rust-async-channel (2.3.1-1) experimental; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * update and unfuzz patches
  * bump project versions in virtual packages and autopkgtests
  * stop (build-)depend on packages for crate event-listener
  * relax to build- and autopkgtest-depend unversioned when version is satisfied in Debian stable

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 10 Jun 2024 11:10:49 +0200

rust-async-channel (2.2.1-1) experimental; urgency=medium

  * update copyright info: fix file path
  * add patch 2001 to accept older branch of crate futures-lite
  * add patch 2002 to avoid WASM-only crates
  * tighten (build-)dependencies
    for crates concurrent-queue event-listener;
    bump (build-)dependencies for crate event-listener;
    relax (build-)dependencies for crate futures-core;
    (Build-)depend on packages
    for for crates event-listener-strategy pin-project-lite;
    relax build- and autopkgtest-dependencies
    for crate futures-lite
  * bump project versions in virtual packages and autopkgtests
  * provide and autopkgtest feature std
  * declare compliance with Debian Policy 4.7.0
  * update dh-cargo fork
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 25 Apr 2024 10:18:02 +0200

rust-async-channel (1.9.0-2) unstable; urgency=medium

  * update dh-cargo fork;
    closes: bug#1046834, thanks to Lucas Nussbaum

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 14 Aug 2023 12:40:20 +0200

rust-async-channel (1.9.0-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * bump version for provided virtual packages and autopkgtest hints

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 16 Jul 2023 11:32:50 +0200

rust-async-channel (1.8.0-3) unstable; urgency=medium

  * tighten autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 02 Feb 2023 17:17:44 +0100

rust-async-channel (1.8.0-2) unstable; urgency=medium

  * drop patch 1001, obsoleted by Debian package updates;
    tighten (build-)dependency on package for crate concurrent-queue
  * declare compliance with Debian Policy 4.6.2
  * update dh-cargo fork
  * update copyright info: update coverage
  * change binary library package to be arch-independent

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 26 Jan 2023 13:04:20 +0100

rust-async-channel (1.8.0-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * update dh-cargo fork;
    build-depend on libstring-shellquote-perl
  * add patch 2001 to relax dependency on crate concurrent-queue
  * bump version for provided virtual packages and autopkgtest hints
  * relax (build-)dependency for crate concurrent-queue

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 05 Dec 2022 17:03:58 +0100

rust-async-channel (1.7.1-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * drop patch 1001 to stop needlessly depend on crate blocking,
    adopted upstream
  * stop overzealously provide versioned virtual packages
  * bump version for provided virtual packages and autopkgtest hints

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 13 Aug 2022 20:39:29 +0200

rust-async-channel (1.6.1-3) unstable; urgency=medium

  * no-changes source-only upload to enable testing migration

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 12 Jul 2022 11:30:34 +0200

rust-async-channel (1.6.1-2) unstable; urgency=medium

  * fix declare runtime dependencies
    (not bogusly provide them)

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 02 Jul 2022 18:30:38 +0200

rust-async-channel (1.6.1-1) unstable; urgency=low

  * initial Release;
    closes: Bug#1014224

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 02 Jul 2022 16:11:10 +0200
